import Head from "next/head";
import Image from "next/image";
import styles from "../styles/page.module.css";
import { ReactNode } from "react";
import { Box, Flex, SimpleGrid } from "@chakra-ui/react";

const Index = () => {
  const itemsId = [
    { name: "Theme1", id: 1 },
    { name: "Theme2", id: 2 },
    { name: "Theme3", id: 3 },
  ];
  return (
    <div className={styles.container}>
      <SimpleGrid columns={4} spacing={10}>
        {itemsId.map((item: any, index: number) => {
          return (
            <Flex key={index} w="100%" height="80px" bg="tomato">
              <Box>{item.name}</Box>
            </Flex>
          );
        })}
      </SimpleGrid>
      {/* 
            <p className={styles.description}>
                Get started by editing{" "}
                <code className={styles.code}>pages/index.tsx</code>
            </p>

            <div className={styles.grid}>
                <a href="https://nextjs.org/docs" className={styles.card}>
                    <h2>Documentation &rarr;</h2>
                    <p>Find in-depth information about Next.js features and API.</p>
                </a>

                <a href="https://nextjs.org/learn" className={styles.card}>
                    <h2>Learn &rarr;</h2>
                    <p>Learn about Next.js in an interactive course with quizzes!</p>
                </a>

                <a
                    href="https://github.com/vercel/next.js/tree/master/examples"
                    className={styles.card}
                >
                    <h2>Examples &rarr;</h2>
                    <p>Discover and deploy boilerplate example Next.js projects.</p>
                </a>

                <a
                    href="https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                    className={styles.card}
                >
                    <h2>Deploy &rarr;</h2>
                    <p>
                        Instantly deploy your Next.js site to a public URL with Vercel.
                    </p>
                </a>
            </div>

            <footer className={styles.footer}>
                <a
                    href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Powered by{' '}
                    <span className={styles.logo}>
                        <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
                    </span>
                </a>
            </footer> */}
    </div>
  );
};

Index.getLayout = (page: ReactNode) => <>{page}</>;

export async function getStaticProps() {
  return {
    props: {},
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 60 * 60 * 24, // In days
  };
}

export default Index;
