"use client";

import { CacheProvider } from "@chakra-ui/next-js";
import { ChakraProvider, theme } from "@chakra-ui/react";
import { ReactNode } from "react";

interface ProvidersProps {
  children: ReactNode;
}

export function Providers({ children }: ProvidersProps) {
  return (
    // <CacheProvider>
      <ChakraProvider theme={theme}>{children}</ChakraProvider>
    // </CacheProvider>
  );
}
